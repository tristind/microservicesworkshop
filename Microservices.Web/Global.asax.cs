﻿#region Includes

using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Daishi.AMQP;
using Microservices.Web;

#endregion

namespace Microservices.Web
{
    public class WebApiApplication : HttpApplication {

        private SimpleMathMicroservice _simpleMathMicroservice;
        private SimpleWordMicroservice _simpleWordMicroservice;


        protected void Application_Start() {

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            # region Microservice Init

            _simpleMathMicroservice = new SimpleMathMicroservice();
            _simpleMathMicroservice.Init();

            _simpleWordMicroservice = new SimpleWordMicroservice();
            _simpleWordMicroservice.Init();

            #endregion

            #region RabbitMQAdapter Init

            RabbitMQAdapter.Instance.Init("localhost", 5672, "guest", "guest", 100);
            RabbitMQAdapter.Instance.Connect();

            #endregion
        }

        protected void Application_End() {

            if (_simpleMathMicroservice != null) {
                _simpleMathMicroservice.Shutdown();
            }

            if (_simpleWordMicroservice != null)
            {
                _simpleWordMicroservice.Shutdown();
            }

            if (RabbitMQAdapter.Instance != null && RabbitMQAdapter.Instance.IsConnected) {
                RabbitMQAdapter.Instance.Disconnect();
            }
        }
    }
}