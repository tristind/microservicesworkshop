﻿#region Includes

using System.Web.Mvc;

#endregion

namespace Microservices.Web {
    public class FilterConfig {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
            filters.Add(new HandleErrorAttribute());
        }
    }
}