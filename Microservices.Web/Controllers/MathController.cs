﻿#region Includes

using System.Net;
using System.Web.Http;
using Daishi.AMQP;
using RabbitMQ.Client.Events;

#endregion

namespace Microservices.Web.Controllers {
    public class MathController : ApiController {
        [Route("api/math/{number}")]
        public string Get(int number) {
            RabbitMQAdapter.Instance.Publish(number.ToString(), "Math");

            string message;
            BasicDeliverEventArgs args;
            var responded = RabbitMQAdapter.Instance.TryGetNextMessage("MathResponse", out message, out args, 5000);

            if (responded)
            {
                return message;
            }
            throw new HttpResponseException(HttpStatusCode.BadGateway);
        }
    }
}