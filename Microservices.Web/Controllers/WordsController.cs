﻿#region Includes

using System.Net;
using System.Web.Http;
using Daishi.AMQP;
using RabbitMQ.Client.Events;

#endregion

namespace Microservices.Web.Controllers {
    public class WordsController : ApiController {
        [Route("api/words/{word}")]
        public string Get(string word) {
            RabbitMQAdapter.Instance.Publish(word, "Word");

            string message;
            BasicDeliverEventArgs args;
            var responded = RabbitMQAdapter.Instance.TryGetNextMessage("WordResponse", out message, out args, 5000);

            if (responded)
            {
                return message;
            }
            throw new HttpResponseException(HttpStatusCode.BadGateway);
        }


    }
}